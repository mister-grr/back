
const express = require('express')
const pg = require('pg');
const cors = require('cors');
const OpenAI = require('openai')
const app = express() // initialize app
const bcrypt = require('bcrypt');
app.use(cors());
app.use(express.json())
app.set('trust_proxy', true)

const saltRounds =10  
const port = 3000
const connectionString='postgres://mister_grr_db_user:EtfzbWIIAEo0Lv6M0kaWLFGz8V4wLFM8@dpg-cn0gti6v3ddc73c1s2pg-a.frankfurt-postgres.render.com:5432/mister_grr_db?ssl=true'
const pool = new pg.Pool({
  connectionString
});

async function login(email, mdp) {
  var rep = ""
  try {
    rep = await pool.query("Select * from utilisateur where email='"+email+"'")
    hash = rep.rows[0]["mdp"]
    bcrypt.compare(mdp, hash, function(err, result) {
      console.log('result :' + result)
      if (result === false) {
        console.log("result === false");
      }
    });
  } catch (error) {
    console.log("Error in login");  
  }
  return rep
}

async function register(email, mdp) {
  console.log("register");
  bcrypt.hash(mdp, saltRounds, async function(err, hash) {
    console.log(hash);
    query = "INSERT INTO utilisateur (email, mdp) VALUES ('"+email+"', '"+hash+"') RETURNING *"
    console.log(query);
    const rep = await pool.query(query)
    console.log(rep);
  })

  return rep
}

const configuration = {
    apiKey: "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"
}

const openai = new OpenAI(configuration)

app.get("/api", (req, res) => {
    res.json({ message: "Hello from server!" });
  });


  app.post("/login", async (req, res) => {
    console.log("login ");
    var result
    try {
      result = await login(req.body["email"], req.body["mdp"])
      
      if (result === false || result.rowCount === 0) {
        res.status(403)
      }
      console.log(result.rows[0]["email"]);
      res.json({ id: result.rows[0]["id"], email: result.rows[0]["email"] });
    } catch (error) {
      console.log("Error while connecting to database from login");
    }
  });

  app.post("/register", async (req, res) => {
    console.log("post  /register");
    console.log(req);
    var result
    try {
      result = await register(req.body["email"], req.body["mdp"])
      res.json({ message: result.rows[0] });
    } catch (error) {
      try {
        if (error.code === '23505') {
          console.log("deja pris");
          res.status(401)
          res.json({message: "Un compte avec cette adresse email existe déjà."})
        }
      } catch (error) {
        console.log("Unknow error : "+ error);
      }
    }
  });


  app.post("/testgptapi", async (req, res) => {
        try {
          console.log(req.headers['true-client-ip'])
          var joueurs=req.body.joueurs
          var questionDejaPassees = req.body.questionsPassees

          var message = 'Joueurs: [' + joueurs +']\
          questionsDejaPassees: [' + questionDejaPassees +']\
          Ecrit 10 questions ou actions de jeu à boire type piccolo, en intégrant entre 1 et 3 joueurs, et différents des questionDejaPassees. Formate le résultat en array de string, sans numéroté les questions'

          const response = await openai.chat.completions.create({
            model: "gpt-3.5-turbo-16k-0613",
            messages: [{ role: "user", content: message }],
            temperature: 0,
            max_tokens: 1000,
          });
 
          questions = JSON.parse(response.choices[0].message.content)
          console.log("Sending...")
          console.log(questions[0])
          res.status(200).json(questions);
        // await delay(3000)
        // res.status(200).json([i,i+1,i+2,i+3,i+4])
        // i+=5
        } catch (err) {
            console.log(err.message)
          res.status(500).json(err.message);
        }
});


const delay = ms => new Promise(
    resolve => setTimeout(resolve, ms)
  );

app.listen(port, () => {
console.log(`Server listening at http://localhost:${port}`)
})
